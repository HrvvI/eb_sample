Rails.application.routes.draw do
  resources :posts
  get 'static_pages/home'
  root 'static_pages#home'
end
